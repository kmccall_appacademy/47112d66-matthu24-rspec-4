class Dictionary
  # TODO: your code goes here!
  attr_reader :entries
  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      @entries.merge!(entry)
    elsif entry.is_a?(String)
      @entries[entry] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def substrings(string)
    sub_strings = []
    string.each_char.with_index do |char,idx|
      (idx..string.length-1).each do |idx2|
        sub_strings << string[idx..idx2]
      end
    end
    sub_strings
  end

  def find(key)
    @entries.select{|k,v| substrings(k).include?(key)}
  end

  def printable
    entries = self.keywords.map do |key|
      %Q{[#{key}] "#{@entries[key]}"}
    end
    entries.join("\n")
  end

end
