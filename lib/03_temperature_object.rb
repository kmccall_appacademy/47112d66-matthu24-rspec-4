class Temperature
  # TODO: your code goes here!
  attr_reader :fahrenheit, :celsius

  def self.from_celsius(temp)
    #pass in a new options hash
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.ctof(temp)
    (temp * 9 / 5.0) + 32
  end

  def self.ftoc(temp)
    (temp - 32) * (5 / 9.0)
  end

  def initialize(degrees)
    if degrees[:f]
      @fahrenheit = degrees[:f]
    else
      @celsius = degrees[:c]
    end
  end

  def in_fahrenheit
    if @celsius != nil
      self.class.ctof(@celsius)
    else
      @fahrenheit
    end
  end

  def in_celsius
    if @fahrenheit != nil
      self.class.ftoc(@fahrenheit)
    else
      @celsius
    end
  end
  def fahrenheit=(temp)
    @fahrenheit = temp
  end

  def celsius=(temp)
    @celsius = temp
  end
end


class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
