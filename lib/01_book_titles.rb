class Book
  # TODO: your code goes here!
  LOWER_WORDS = [
    "the",
    "a",
    "an",
    "and",
    "in",
    "of"
  ]
  attr_reader :title
  def title= (title)
    title_array = title.split(" ").each_with_index do |word,idx|
      word.capitalize! unless (LOWER_WORDS.include?(word) && idx != 0)
    end
    @title = title_array.join(" ")
  end

end
