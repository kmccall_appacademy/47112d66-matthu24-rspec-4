class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  #adds a zero if num is less than 10
  def padded(num)
    if num < 10
      return "0#{num}"
    else
      return "#{num}"
    end
  end

  def hours
    @seconds/3600
  end

  def minutes
    (@seconds%3600)/60
  end

  def second
    (@seconds%3600)%60
  end

  def time_string
    "#{padded(hours)}:#{padded(minutes)}:#{padded(second)}"
  end

end
